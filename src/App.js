import { Steps } from './components/Steps';
import { Header } from './components/Header';
import { Footer } from './components/Footer';

import './scss/App.scss';

function App() {
  return (
    <div className="App">
      <Header />
      <Steps />
      <Footer />
    </div>
  );
}

export default App;
