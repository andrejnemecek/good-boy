const SHELTER_API_HOST =
  'https://frontend-assignment-api.goodrequest.com/api/v1';

class ShelterApi {
  constructor(host) {
    this.SHELTER_API_HOST = host;
  }

  async fetchShelters() {
    const response = await fetch(`${this.SHELTER_API_HOST}/shelters`);
    const responseData = await response.json();

    return responseData.shelters;
  }

  async sendForm(formData) {
    const response = await fetch(
      `${this.SHELTER_API_HOST}/shelters/contribute`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
        body: JSON.stringify(formData),
      }
    );

    if (response.ok) {
      const responseData = await response.json();
      console.log(`HTTP-Success: ${responseData}`);
    } else {
      console.log('HTTP-Error: ' + response.status);
    }

    return response;
  }
}

export const shelterApi = new ShelterApi(SHELTER_API_HOST);
