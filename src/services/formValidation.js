export const isEmpty = (inputValue) => {
  if (inputValue.trim() === '') return true;
  return false;
};

export const isLength = (inputValue, minLength = 2, maxLength = 20) => {
  if (inputValue.trim().length < minLength) {
    return false;
  }

  if (inputValue.trim().length > maxLength) {
    return false;
  }

  return true;
};

export const isValidEmail = (inputValue) => {
  if (!/^\S+@\S+\.\S+$/.test(inputValue)) return false;

  return true;
};

export const isValidPhone = (inputValue) => {
  if (!/^\d+$/.test(inputValue)) return false;

  return true;
};
