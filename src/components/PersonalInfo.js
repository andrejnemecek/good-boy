import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  setFirstName,
  setLastName,
  setEmail,
  setPhonePrefix,
  setPhone,
} from '../redux/actions/donationForm';
import { PHONE_PREFIX_DEFAULT_VALUE } from '../redux/constants';
import {
  isEmpty,
  isValidEmail,
  isValidPhone,
  isLength,
} from '../services/formValidation';
import czFlag from '../images/flag-cz.png';

export const PersonalInfo = ({ nextStep, previousStep }) => {
  const reduxDispatch = useDispatch();
  const firstName = useSelector((state) => state.firstName);
  const lastName = useSelector((state) => state.lastName);
  const email = useSelector((state) => state.email);
  const phonePrefix = useSelector((state) => state.phonePrefix);
  const phone = useSelector((state) => state.phone);

  const changeFirstName = (event) => {
    reduxDispatch(setFirstName(event.target.value));
  };
  const changeLastName = (event) => {
    reduxDispatch(setLastName(event.target.value));
  };
  const changeEmail = (event) => {
    reduxDispatch(setEmail(event.target.value));
  };
  const changePhonePrefix = (phonePrefix) => {
    reduxDispatch(setPhonePrefix(phonePrefix));
    tooglePhonePrefixDropDown();
  };
  const changePhone = (event) => {
    reduxDispatch(setPhone(event.target.value));
  };

  const phonePrefixList = [
    PHONE_PREFIX_DEFAULT_VALUE,
    { id: 2, value: '+420', flag: czFlag },
  ];
  const [isPhonePrefixDropDownOpen, setIsPhonePrefixDropDownOpen] = useState(
    false
  );
  const tooglePhonePrefixDropDown = () => {
    setIsPhonePrefixDropDownOpen(!isPhonePrefixDropDownOpen);
  };

  const [firstNameError, setFirstNameError] = useState(false);
  const [lastNameError, setLastNameError] = useState(false);
  const [emailError, setEmailError] = useState(false);
  const [phoneError, setPhoneError] = useState(false);

  const validateFirstName = () => {
    if (firstName === '') {
      setFirstNameError(false);
      return true;
    }

    if (!isLength(firstName)) {
      setFirstNameError('Meno musí obsahovať 2 až 20 znakov');
      return false;
    }

    setFirstNameError(false);
    return true;
  };

  const validateLastName = () => {
    if (isEmpty(lastName)) {
      setLastNameError('Vyplňte svoje priezvisko');
      return false;
    }

    if (!isLength(lastName)) {
      setLastNameError('Priezvisko musí obsahovať 2 až 20 znakov');
      return false;
    }

    setLastNameError(false);
    return true;
  };

  const validateEmail = () => {
    if (isEmpty(email)) {
      setEmailError('Vyplňte svoj email');
      return false;
    }

    if (!isValidEmail(email)) {
      setEmailError('Vyplňte existujúci email');
      return false;
    }

    setEmailError(false);
    return true;
  };

  const validatePhone = () => {
    if (phone === '') {
      setPhoneError(false);
      return true;
    }

    if (!isValidPhone(phone)) {
      setPhoneError('Vyplňte existujúci telefónne číslo');
      return false;
    }

    setPhoneError(false);
    return true;
  };

  const switchNextStep = () => {
    let haveError = false;
    if (!validateFirstName()) haveError = true;
    if (!validateLastName()) haveError = true;
    if (!validateEmail()) haveError = true;
    if (!validatePhone()) haveError = true;

    if (haveError) return;
    nextStep();
  };

  return (
    <div className="step app-container">
      <section className="step__content py-4">
        <div className="step-indicator">
          <div className="step-indicator__item"></div>
          <div className="step-indicator__item step-indicator__item--active"></div>
          <div className="step-indicator__item"></div>
        </div>
        <h1>Potrebujeme od Vás zopár informácií</h1>
        <form>
          <div className="input-description">
            <div>O vás</div>
          </div>
          <div className="form-field">
            <div className="form-field__input-block">
              <label className="form-field__label" htmlFor="firstName">
                Meno
              </label>
              <input
                className="form-field__input"
                type="text"
                name="firstName"
                id="firstName"
                placeholder="Zadajte Vaše meno"
                value={firstName}
                onChange={changeFirstName}
                onBlur={validateFirstName}
              />
            </div>
            <div className="input-error">{firstNameError}</div>
          </div>

          <div className="form-field">
            <div className="form-field__input-block">
              <label className="form-field__label" htmlFor="lastName">
                Priezvisko
              </label>
              <input
                className="form-field__input"
                type="text"
                name="lastName"
                id="lastName"
                placeholder="Zadajte Vaše priezvisko"
                value={lastName}
                onChange={changeLastName}
                onBlur={validateLastName}
              />
            </div>
            <div className="input-error">{lastNameError}</div>
          </div>

          <div className="form-field">
            <div className="form-field__input-block">
              <label className="form-field__label" htmlFor="email">
                E-mailová adresa
              </label>
              <input
                className="form-field__input"
                type="email"
                name="email"
                id="email"
                placeholder="Zadajte Váš e-mail"
                value={email}
                onChange={changeEmail}
                onBlur={validateEmail}
              />
            </div>
            <div className="input-error">{emailError}</div>
          </div>
          <div className="form-field">
            <div className="form-field__input-block">
              <label className="form-field__label" htmlFor="phone">
                Telefónne číslo
              </label>
              <div className="form-field__prefix">
                <div onClick={tooglePhonePrefixDropDown}>
                  <img
                    className="form-field__flag"
                    src={phonePrefix.flag}
                    alt=""
                  />
                  {phonePrefix.value}
                </div>
                <div className="dropdown">
                  <ul
                    className={`dropdown__list dropdown__list--flags ${
                      isPhonePrefixDropDownOpen ? 'dropdown__list--open' : ''
                    }`}
                  >
                    {phonePrefixList.map((prefix) => (
                      <li
                        key={prefix.id}
                        className="dropdown__item"
                        onClick={() => {
                          changePhonePrefix(prefix);
                        }}
                      >
                        <img
                          className="form-field__flag"
                          src={prefix.flag}
                          alt=""
                        />
                        {prefix.value}
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
              <input
                className="form-field__input form-field__input--with-prefix"
                type="tel"
                name="phone"
                id="phone"
                value={phone}
                onChange={changePhone}
                onBlur={validatePhone}
              />
            </div>
            <div className="input-error">{phoneError}</div>
          </div>
        </form>

        <div className="mt-3 step-footer">
          <button
            className="button step-footer__back-button"
            onClick={previousStep}
          >
            Spať
          </button>
          <button
            className="button step-footer__next-button"
            onClick={switchNextStep}
          >
            Pokračovať
          </button>
        </div>
      </section>
      <div className="step__bg-image "></div>
    </div>
  );
};
