import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  setDonateWholeOrganization,
  setShelter,
  setDonationPrice,
} from '../redux/actions/donationForm';
import { DEFAULT_VALUE } from '../redux/constants';
import { shelterApi } from '../services/apiService';

import { ReactComponent as PawIcon } from '../images/paw.svg';
import { ReactComponent as WalletIcon } from '../images/wallet.svg';
import { ReactComponent as DropDownIcon } from '../images/dropdown.svg';

export const DonationInfo = ({ nextStep }) => {
  const reduxDispatch = useDispatch();

  const donateWholeOrganization = useSelector(
    (state) => state.donateWholeOrganization
  );

  const shelter = useSelector((state) => state.shelter);
  const [sheltersList, setSheltersList] = useState([]);
  const [selectedShelter, setSelectedShelter] = useState(null);

  useEffect(() => {
    const fetchShelters = async () => {
      const fetchedSheltersList = await shelterApi.fetchShelters();
      setSheltersList(fetchedSheltersList);
    };

    fetchShelters();
  }, []);

  useEffect(() => {
    if (shelter) {
      setSelectedShelter(shelter.name);
    }
  }, [shelter]);

  const changeShelter = (id) => {
    const shelter = sheltersList.find((shelter) => shelter.id === id);
    reduxDispatch(setShelter(shelter));
    setIsShelterDropDownOpen(false);
  };

  const [isShelterDropDownOpen, setIsShelterDropDownOpen] = useState(false);
  const toogleShelterDropDown = () => {
    setIsShelterDropDownOpen(!isShelterDropDownOpen);
  };

  const pricesList = [
    DEFAULT_VALUE,
    { id: 2, value: 10 },
    { id: 3, value: 20 },
    { id: 4, value: 30 },
    { id: 5, value: 50 },
    { id: 6, value: 100 },
  ];
  const selectedPrice = useSelector((state) => state.selectedPrice);
  const changeDonationPrice = (donationPrice) => {
    reduxDispatch(setDonationPrice(donationPrice));
  };

  const [shelterError, setShelterError] = useState(false);

  const validateSelectShelter = () => {
    if (!donateWholeOrganization && !shelter) {
      setShelterError('Vyberte konkrétny útulok');
      return false;
    }

    setShelterError(false);
    return true;
  };

  const switchNextStep = () => {
    if (!validateSelectShelter()) return;

    nextStep();
  };

  return (
    <div className="step app-container">
      <section className="step__content py-4">
        <div className="step-indicator">
          <div className="step-indicator__item  step-indicator__item--active"></div>
          <div className="step-indicator__item"></div>
          <div className="step-indicator__item"></div>
        </div>
        <h1>Vyberte si možnosť, ako chcete pomôcť</h1>
        <div className="donate-selector">
          <button
            className={`donate-selector__button donate-selector__button--first ${
              donateWholeOrganization ? '' : 'donate-selector__button--active'
            }`}
            onClick={() => {
              reduxDispatch(setDonateWholeOrganization(false));
            }}
          >
            <div className="donate-selector__icon">
              <WalletIcon height="30" width="30" />
            </div>
            <div className="mt-1 donate-selector__text">
              Chcem finančne prispieť konkrétnemu útulku
            </div>
          </button>
          <button
            className={`donate-selector__button donate-selector__button--second ${
              donateWholeOrganization ? 'donate-selector__button--active' : ''
            }`}
            onClick={() => {
              reduxDispatch(setDonateWholeOrganization(true));
            }}
          >
            <div className="donate-selector__icon">
              <PawIcon height="30" width="30" />
            </div>
            <div className="mt-1 donate-selector__text">
              Chcem finančne prispieť celej nadácii
            </div>
          </button>
        </div>
        <div className="mt-3 dropdown">
          <div className="input-description">
            <div>Najviac mi záleží na útulku</div>
            <div>{donateWholeOrganization ? 'Nepovinné' : 'Povinné'}</div>
          </div>
          <button className="dropdown-input" onClick={toogleShelterDropDown}>
            <div>
              <div className="dropdown-input__label">Útulok</div>
              <div className="dropdown-input__placeholder">
                {selectedShelter
                  ? selectedShelter
                  : 'Vyberte útulok zo zoznamu'}
              </div>
            </div>
            <div>
              <DropDownIcon />
            </div>
          </button>
          <div className="input-error">{shelterError}</div>
          <ul
            className={`dropdown__list dropdown__list--long ${
              isShelterDropDownOpen ? 'dropdown__list--open' : ''
            }`}
          >
            {sheltersList.map((shelter) => (
              <li
                className="dropdown__item"
                key={shelter.id}
                onClick={() => {
                  changeShelter(shelter.id);
                }}
              >
                {shelter.name}
              </li>
            ))}
          </ul>
        </div>
        <div className="mt-2 input-description">
          <div>Suma, ktorou chcem prispieť</div>
        </div>
        <ul className="price-selector">
          {pricesList.map((price) => (
            <li
              key={price.id}
              onClick={() => {
                changeDonationPrice(price);
              }}
            >
              <button
                className={`price-selector__button ${
                  selectedPrice.id === price.id
                    ? 'price-selector__button--active'
                    : ''
                }`}
              >{`${price.value} €`}</button>
            </li>
          ))}
          <li>
            <div
              className={`price-selector__button ${
                selectedPrice.id === null
                  ? 'price-selector__button--active'
                  : ''
              }`}
            >
              <input
                className={`price-selector__input ${
                  selectedPrice.id === null
                    ? 'price-selector__input--active'
                    : ''
                }`}
                type="number"
                name="price"
                id="price"
                min="1"
                value={selectedPrice.id === null ? selectedPrice.value : ''}
                onChange={(event) => {
                  changeDonationPrice({ id: null, value: event.target.value });
                }}
              />
              <div>€</div>
            </div>
          </li>
        </ul>
        <div className="mt-3 step-footer">
          <button
            className="button step-footer__next-button"
            onClick={switchNextStep}
          >
            Pokračovať
          </button>
        </div>
      </section>
      <div className="step__bg-image "></div>
    </div>
  );
};
