import React from 'react';

import logo from '../images/logo_goodboy.png';

export const Footer = () => {
  return (
    <div className="app-container">
      <footer className="mt-2 app-footer">
        <div className="app-footer__column">
          <img src={logo} alt="Logo Good boy" />
        </div>
        <div className="app-footer__column">
          <p className="app-footer__heading">Nadácia Good boy</p>
          <ul className="app-footer__nav">
            <li>
              <a className="app-footer__link" href="/">
                O projekte
              </a>
            </li>
            <li>
              <a className="app-footer__link" href="/">
                Ako na to
              </a>
            </li>
            <li>
              <a className="app-footer__link" href="/">
                Kontakt
              </a>
            </li>
          </ul>
        </div>
        <div className="app-footer__column">
          <p className="app-footer__heading">Nadácia Good boy</p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus in
            interdum ipsum, sit amet.
          </p>
        </div>
        <div className="app-footer__column">
          <p className="app-footer__heading">Nadácia Good boy</p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus in
            interdum ipsum, sit amet.
          </p>
        </div>
      </footer>
    </div>
  );
};
