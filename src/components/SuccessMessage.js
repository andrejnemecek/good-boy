import React from 'react';

export const SuccessMessage = () => {
  const reloadPage = () => {
    window.location.reload();
  };

  return (
    <div className="step app-container">
      <section className="py-4">
        <h1>Príspevok bol úspešne zaznamenaný</h1>
        <button
          className="button step-footer__next-button"
          onClick={reloadPage}
        >
          Pokračovať
        </button>
      </section>
    </div>
  );
};
