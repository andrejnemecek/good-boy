import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { shelterApi } from '../services/apiService';

export const Confirmation = ({ previousStep, nextStep }) => {
  const donateWholeOrganization = useSelector(
    (state) => state.donateWholeOrganization
  );
  const shelter = useSelector((state) => state.shelter);
  const selectedPrice = useSelector((state) => state.selectedPrice.value);
  const firstName = useSelector((state) => state.firstName);
  const lastName = useSelector((state) => state.lastName);
  const email = useSelector((state) => state.email);
  const phonePrefix = useSelector((state) => state.phonePrefix.value);
  const phone = useSelector((state) => state.phone);

  const [confirmationState, setConfirmationState] = useState(false);
  const [confirmationError, setConfirmationError] = useState(false);

  const validateConfirmation = () => {
    if (!confirmationState) {
      setConfirmationError('Súhlas so spracovaním údajov je povinný');
      return false;
    }

    setConfirmationError(true);
    return true;
  };

  const changeConfirmation = () => {
    if (confirmationState === false) setConfirmationError(false);
    setConfirmationState(!confirmationState);
  };

  const [responseError, setResponseError] = useState(false);

  const sendForm = async () => {
    if (!validateConfirmation()) return;

    const finalObject = {
      firstName,
      lastName,
      email,
      phone: phone === '' ? undefined : `${phonePrefix}${phone}`,
      value: selectedPrice,
      shelterID: shelter?.id,
    };

    const response = await shelterApi.sendForm(finalObject);
    if (response.ok) {
      nextStep();
    } else {
      setResponseError(
        `Server zamietol zaevidovať formulár s chybou: ${response.status}`
      );
    }
  };

  return (
    <div className="step app-container">
      <section className="step__content py-4">
        <div className="step-indicator">
          <div className="step-indicator__item"></div>
          <div className="step-indicator__item"></div>
          <div className="step-indicator__item step-indicator__item--active"></div>
        </div>
        <h1>Skontrolujte si zadané údaje</h1>
        <section className="confirmation">
          <ul className="confirmation__list">
            <li className="confirmation__field">
              <span className="confirmation__label">
                Akou formou chcem pomôcť
              </span>
              {donateWholeOrganization
                ? 'Chcem finančne prispieť celej nadácii'
                : 'Chcem finančne prispieť konkrétnemu útulku'}
            </li>
            <li className="confirmation__field">
              <span className="confirmation__label">
                Najviac mi záleží na útulku
              </span>
              {shelter ? shelter.name : ''}
            </li>
            <li className="confirmation__field">
              <span className="confirmation__label">
                Suma ktorou chcem pomôcť
              </span>
              {`${selectedPrice} €`}
            </li>
            <li className="confirmation__field">
              <span className="confirmation__label">Meno a priezvisko</span>
              {`${firstName} ${lastName}`}
            </li>
            <li className="confirmation__field">
              <span className="confirmation__label">E-mailová adresa</span>
              {email}
            </li>
            <li className="confirmation__field">
              <span className="confirmation__label">Telefónne číslo</span>
              {phone === '' ? '' : `${phonePrefix}${phone}`}
            </li>
          </ul>
          <div className="mt-2">
            <label className="checkbox">
              <input
                className="checkbox__input"
                type="checkbox"
                name="agreement"
                id="agreement"
                checked={confirmationState}
                onChange={changeConfirmation}
              />
              <span className="checkbox__label">
                Súhlasím so spracovaním mojich osobných údajov
              </span>
            </label>
            <div className="input-error">{confirmationError}</div>
          </div>
        </section>
        <div className="mt-3 step-footer">
          <button
            className="button step-footer__back-button"
            onClick={previousStep}
          >
            Spať
          </button>
          <button
            className="button step-footer__next-button"
            onClick={sendForm}
          >
            Odoslať formulár
          </button>
        </div>
        <div className="error-message">{responseError}</div>
      </section>
      <div className="step__bg-image "></div>
    </div>
  );
};
