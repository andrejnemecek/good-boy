import React from 'react';

import facebookLogo from '../images/facebook.svg';
import instagramLogo from '../images/instagram.svg';

export const Header = () => {
  return (
    <header className="app-header mb-2">
      <div className="app-header__content app-container">
        <div className="app-header__logo">Nadácia Good Boy</div>
        <nav className="app-header__nav">
          <img
            className="app-header__link"
            src={facebookLogo}
            alt="Odkaz na Facebook"
          />
          <img
            className="app-header__link"
            src={instagramLogo}
            alt="Odkaz na Instagram"
          />
        </nav>
      </div>
    </header>
  );
};
