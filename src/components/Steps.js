import { React, useState } from 'react';
import { Confirmation } from './Confirmation';
import { DonationInfo } from './DonationInfo';
import { PersonalInfo } from './PersonalInfo';
import { SuccessMessage } from './SuccessMessage';

export const Steps = () => {
  const [actualStep, setActualStep] = useState(0);

  const switchToNextStep = () => {
    setActualStep(actualStep + 1);
  };

  const switchToPreviousStep = () => {
    setActualStep(actualStep - 1);
  };

  switch (actualStep) {
    case 0:
      return <DonationInfo nextStep={switchToNextStep} />;
    case 1:
      return (
        <PersonalInfo
          nextStep={switchToNextStep}
          previousStep={switchToPreviousStep}
        />
      );
    case 2:
      return (
        <Confirmation
          nextStep={switchToNextStep}
          previousStep={switchToPreviousStep}
        />
      );
    case 3:
      return <SuccessMessage />;
    default:
      break;
  }
};
