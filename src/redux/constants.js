import skFlag from '../images/flag-sk.png';

export const CHANGE_DONATE_WHOLE_ORGANIZATION =
  'CHANGE_DONATE_WHOLE_ORGANIZATION';
export const CHANGE_SHELTER = 'CHANGE_SHELTER';
export const CHANGE_PRICE = 'CHANGE_PRICE';
export const CHANGE_FIRST_NAME = 'CHANGE_FIRST_NAME';
export const CHANGE_LAST_NAME = 'CHANGE_LAST_NAME';
export const CHANGE_EMAIL = 'CHANGE_EMAIL';
export const CHANGE_PHONE_PREFIX = 'CHANGE_PHONE_PREFIX';
export const CHANGE_PHONE = 'CHANGE_PHONE';

export const DEFAULT_VALUE = { id: 1, value: 5 };
export const PHONE_PREFIX_DEFAULT_VALUE = {
  id: 1,
  value: '+421',
  flag: skFlag,
};
