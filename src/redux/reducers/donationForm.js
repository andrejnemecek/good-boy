import {
  CHANGE_DONATE_WHOLE_ORGANIZATION,
  CHANGE_SHELTER,
  CHANGE_PRICE,
  CHANGE_FIRST_NAME,
  CHANGE_LAST_NAME,
  CHANGE_EMAIL,
  CHANGE_PHONE_PREFIX,
  CHANGE_PHONE,
  DEFAULT_VALUE,
  PHONE_PREFIX_DEFAULT_VALUE,
} from '../constants';

const donationFormFields = {
  firstName: '',
  lastName: '',
  email: '',
  phonePrefix: PHONE_PREFIX_DEFAULT_VALUE,
  phone: '',
  donateWholeOrganization: true,
  selectedPrice: DEFAULT_VALUE,
  shelter: null,
};

export const donationFormReducer = (state = donationFormFields, action) => {
  switch (action.type) {
    case CHANGE_DONATE_WHOLE_ORGANIZATION:
      return { ...state, donateWholeOrganization: action.value };

    case CHANGE_SHELTER:
      return { ...state, shelter: action.value };

    case CHANGE_PRICE:
      return { ...state, selectedPrice: action.value };

    case CHANGE_FIRST_NAME:
      return { ...state, firstName: action.value };

    case CHANGE_LAST_NAME:
      return { ...state, lastName: action.value };

    case CHANGE_EMAIL:
      return { ...state, email: action.value };

    case CHANGE_PHONE_PREFIX:
      return { ...state, phonePrefix: action.value };

    case CHANGE_PHONE:
      return { ...state, phone: action.value };

    default:
      return state;
  }
};
