import {
  CHANGE_DONATE_WHOLE_ORGANIZATION,
  CHANGE_SHELTER,
  CHANGE_PRICE,
  CHANGE_FIRST_NAME,
  CHANGE_LAST_NAME,
  CHANGE_EMAIL,
  CHANGE_PHONE_PREFIX,
  CHANGE_PHONE,
} from '../constants';

export const setDonateWholeOrganization = (donateWholeOrganization) => {
  return {
    type: CHANGE_DONATE_WHOLE_ORGANIZATION,
    value: donateWholeOrganization,
  };
};

export const setShelter = (shelter) => {
  return {
    type: CHANGE_SHELTER,
    value: shelter,
  };
};

export const setDonationPrice = (value) => {
  return {
    type: CHANGE_PRICE,
    value: value,
  };
};

export const setFirstName = (firstName) => {
  return {
    type: CHANGE_FIRST_NAME,
    value: firstName,
  };
};

export const setLastName = (lastName) => {
  return {
    type: CHANGE_LAST_NAME,
    value: lastName,
  };
};

export const setEmail = (email) => {
  return {
    type: CHANGE_EMAIL,
    value: email,
  };
};

export const setPhonePrefix = (phonePrefix) => {
  return {
    type: CHANGE_PHONE_PREFIX,
    value: phonePrefix,
  };
};

export const setPhone = (phone) => {
  return {
    type: CHANGE_PHONE,
    value: phone,
  };
};
